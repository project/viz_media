<?php

namespace Drupal\viz_media;

use Drupal\Core\Access\AccessResult;

class MediaAccess {

  public function checkCreate() {
    if (\Drupal::currentUser()->isAnonymous()) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

  public function checkView() {
    return AccessResult::allowed();
  }

}
