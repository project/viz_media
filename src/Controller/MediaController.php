<?php

namespace Drupal\viz_media\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;
use Drupal\viz_media\Utils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MediaController extends ControllerBase {

  /**
   * @var \Drupal\media\MediaStorage
   */
  protected $storage;

  public function __construct() {
    /** @var \Drupal\media\MediaStorage $storage */
    $this->storage = \Drupal::entityTypeManager()->getStorage('media');
  }

  public function getMedia($param) {
    if (is_numeric($param)) {
      return $this->getSingleMedia($param);
    }
    return $this->getMediaList($param);
  }

  public function getSingleMedia($param) {
    /** @var Media $media */
    $media = $this->storage->load($param);
    if (!$media) {
      throw new \Exception('Not Found');
    }
    $data = $this->buildData($media);
    return new JsonResponse($data);
  }

  public function getMediaList($type = Null) {
    $request = \Drupal::request();

    $page = $request->get('page', 0);
    $limit = $request->get('limit', 20);

    $query = \Drupal::entityQuery('media');
    $query->accessCheck(True);

    if (!is_null($type)) {
      $types = explode(' ', $type);
      $query->condition('bundle', $types, 'IN');
    }

    $query->sort('changed', 'DESC');

    $keyword = $request->get('keyword', null);
    if ($keyword) {
      $query->condition('keyword', "%" . \Drupal::database()->escapeLike($keyword) . "%", 'LIKE');
    }

    $tag_id = $request->get('tagId');
    if ($tag_id) {
      $query->condition('field_tags', $tag_id);
    }

    $query->pager($limit);
    $countQuery = clone $query;
    $total = $countQuery->count()->execute();
    $ids = $query->execute();

    foreach ($ids as $id) {
      $media = $this->storage->load($id);
      $list[] =$this->buildData($media);
    }

    $data = [
      'page' => (int) $page,
      'pageSize' => (int) $limit,
      'total' => (int) $total,
      'list' => !empty($list) ? $list : []
    ];

    return new JsonResponse($data);
  }

  public function createMedia($media_type) {
    if (!$this->isSupported($media_type)) {
      throw new AccessDeniedHttpException('Type not supported');
    }

    $request = \Drupal::request();
    $input = $request->getContent();
    $data = json_decode($input, TRUE);

    // mandatory fields
    if (empty($data['name']) || empty($data['fid'])) {
      throw new BadRequestHttpException('Missing fields');
    }

    try {
      // Solved deadlock issue on batch upload
      // \Drupal::database()->query("SET SESSION transaction_isolation='READ-COMMITTED'")->execute();

      /** @var Media $media */
      $media = $this->storage->create([
        'bundle' => $media_type,
        'name' => $data['name'],
      ]);

      if (array_key_exists('media_length', $data)) {
        $media->field_media_length->setValue($data['media_length']);
      }
      if (array_key_exists('tag_id', $data) && !empty($data['tag_id'])) {
        @$media->field_tags->setValue($data['tag_id']);
      }
      if (isset($data['keyword']) && !empty($data['keyword'])) {
        $media->keyword->setValue($data['keyword']);
      }

      $source = $media->getSource();
      $source_config = $source->getConfiguration();
      $media->get($source_config['source_field'])->setValue($data['fid']);

      $access = $media->access('create');
      if (!$access) {
        throw new AccessDeniedHttpException('No Permission');
      }

      $media->save();
      /** @var Media $saved_media */
      $saved_media = $this->storage->load($media->id());

      if ($thumbnail = Utils::generateThumbnail($data['fid'])) {
        $saved_media->get('thumbnail')->setValue($thumbnail);
      }
      $saved_media->save();

      $data = $this->buildData($saved_media);

      return new JsonResponse($data, 201);
    }
    catch (\Exception $exception) {
      return new JsonResponse([
        'errCode' => $exception->getCode(),
        'errMsg' => $exception->getMessage(),
      ]);
    }
  }

  public function updateMedia($media) {
    /** @var Media $media */
    if (!$media) {
      throw new NotFoundHttpException('media not found');
    }
    $request = \Drupal::request();
    $input = $request->getContent();
    $data = json_decode($input, TRUE);

    try {
      // \Drupal::database()->query("SET SESSION transaction_isolation='READ-COMMITTED'")->execute();

      if (array_key_exists('name', $data)) {
        $media->setName($data['name']);
      }
      if (array_key_exists('fid', $data)) {
        $file = File::load($data['fid']);
        $mimetype = explode('/', $file->getMimeType())[0];
        //TODO Temporarily ignore
        /*if ($mimetype != $media->bundle()) {
          throw new BadRequestHttpException('source not matched with media type', null, 500);
        }*/
        $source_config = $media->getSource()->getConfiguration();
        $media->get($source_config['source_field'])->setValue($data['fid']);
        if ($thumbnail = Utils::generateThumbnail($data['fid'])) {
          $media->get('thumbnail')->setValue($thumbnail);
        }
      }
      if (!empty($data['media_length'])) {
        $media->field_media_length->setValue($data['media_length']);
      }
      if (array_key_exists('tag_id', $data)) {
        @$media->field_tags->setValue($data['tag_id']);
      }
      if (isset($data['keyword']) && !empty($data['keyword'])) {
        $media->keyword->setValue($data['keyword']);
      }
      $media->save();
      return new JsonResponse($this->buildData($media));
    }
    catch (\Exception $exception) {
      return new JsonResponse([
        'errCode' => $exception->getCode(),
        'errMsg' => $exception->getMessage(),
      ]);
    }
  }

  public function deleteMedia($media) {
    if (!$media) {
      throw new NotFoundHttpException();
    }

    try {
      \Drupal::database()->query("SET SESSION transaction_isolation='READ-COMMITTED'")->execute();
      $media->delete();
      return new JsonResponse(1, 204);
    }
    catch (\Exception $exception) {
      return new JsonResponse([
        'errCode' => $exception->getCode(),
        'errMsg' => $exception->getMessage(),
      ]);
    }
  }

  protected function buildData(Media $media) {
    $data = [
      'mid' => (int) $media->id(),
      'uuid' => $media->uuid(),
      'name' => $media->getName(),
      'type' => $media->bundle(),
      'field_tags' => [],
      'created' => date('Y-m-d H:i', $media->getCreatedTime()),
      'changed' => date('Y-m-d H:i', $media->getChangedTime()),
    ];

    $source = $media->getSource();
    $source_config = $source->getConfiguration();
    /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
    $field_item = $media->get($source_config['source_field']);
    if ($file = $field_item->entity) {
      $data['fid'] = (int) $file->id();
      $data['fuuid'] = $file->uuid();
      $data['url'] = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
      $data['relative_url'] = \Drupal::service('file_url_generator')->generateString($file->getFileUri());
      $data['width'] = (int) $field_item->width;
      $data['height'] = (int) $field_item->height;
    }

    $keys = array_keys($media->toArray());

    if (in_array('field_tags', $keys)) {
      foreach ($media->field_tags as $tag) {
        if ($tag->entity) {
          $data['field_tags'][] = [
            'id' => (int) $tag->entity->id(),
            'name' => $tag->entity->label(),
          ];
        }
      }
    }


    if ($thumbnail = $media->thumbnail->entity) {
      $image_style = ImageStyle::load('medium');
      $uri = $thumbnail->getFileUri();
      $data['thumbnail'] = [
        'fid' => $thumbnail->id(),
        'fuuid' => $thumbnail->uuid(),
        'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($uri),
        'relative_url' => \Drupal::service('file_url_generator')->generateString($uri),
        'width' => (int) $media->thumbnail->width,
        'height' => (int) $media->thumbnail->height,
      ];
      $image_style->transformDimensions($data['thumbnail'], $uri);
    }
    else {
      $data['thumbnail'] = [];
    }

    return $data;
  }

  public function fileUpload() {
    $current = \Drupal::currentUser();
    if ($current->isAnonymous()) {
      // return new JsonResponse(['errMsg' => 'No Permission'], 400);
    }

    $date_dir = date('Ym');
    $request = \Drupal::request();

    if ($upload_directory = $request->request->get('upload_dir')) {
      $destination = 'public://' . $upload_directory . '/' . $date_dir;
    }
    else {
      $destination = 'public://media/' . $date_dir;
    }
    \Drupal::service('file_system')->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY);

    $validators = [];
    $extensions = 'jpg jpeg png gif doc docx xls xlsx ppt pptx pdf txt zip rar 7z mp4';
    $validators['file_validate_extensions'] = [];
    $validators['file_validate_extensions'][0] = $extensions;

    $file = file_save_upload('0', $validators, $destination, 0);
    if ($file instanceof File) {
      $original_filename = $file->getFilename();
      //      $file->setPermanent();
      $parts = explode('.', $file->getFilename());
      $ext = array_pop($parts);
      // need to call file_unmunge_filename() ?
      ini_set('precision', 16);
      $microtime = microtime(true) / 0.000001;
      $new_file_uri = $destination . '/' . $microtime . '.' . $ext;
      $file = \Drupal::service('file.repository')->move($file, $new_file_uri, FileSystemInterface::EXISTS_RENAME);
      $file->setFilename($original_filename);
      if ($request->get('permanent', FALSE)) {
        $file->setPermanent();
      }
      $file->save();

      $res = [
        'fid' => $file->id(),
        'fuuid' => $file->uuid(),
        'name' => implode(' ', $parts),
        'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri()),
        'relative_url' => \Drupal::service('file_url_generator')->generateString($file->getFileUri())
      ];

      return new JsonResponse($res);
    }
    else {
      return new JsonResponse(['errMsg' => '上传失败'], 400);
    }
  }

  protected function isSupported($type) {
    $supported_types = [
      'image',
      'video',
      'audio',
      'file',
    ];

    return in_array($type, $supported_types);
  }

}
