<?php

namespace Drupal\viz_media\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class MediaTagController.
 */
class MediaTagController extends ControllerBase {

  /**
   * Media Tag Vocabulary ID
   */
  const MEDIA_TAG_VID = 'media_tags';

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityQuery;

  /**
   * Constructs a new MediaController object.
   */
  public function __construct(CurrentRouteMatch $current_route_match) {
    $this->currentRouteMatch = $current_route_match;

    $this->storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $this->entityQuery = \Drupal::entityQuery('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  public function getMediaTag() {

    $ids = $this->entityQuery
      ->accessCheck(TRUE)
      ->condition('vid', self::MEDIA_TAG_VID)
      ->sort('weight')
      ->execute();
    $terms = $this->storage->loadMultiple($ids);

    $tags = [];
    foreach ($terms as $term) {
      $tags[] = self::serialize($term);
    }
    $tree = $this->buildTree($tags);

    return new JsonResponse($tree);
  }

  public function createMediaTag() {

    $data = $this->getRequestData();
    $term = $this->storage->create([
      'vid' => self::MEDIA_TAG_VID,
      'name' => $data['name'],
      'parent' => $data['pid'] ?: 0,
      'weight' => $data['weight'] ?: 0,
    ]);
    $term->save();

    return new JsonResponse(['errCode' => 0, 'data' => self::serialize($term)]);
  }

  public function updateMediaTag($media_tag) {
    $data = $this->getRequestData();

    if (!$media_tag) {
      return new JsonResponse(['errCode' => 404, 'errMsg' => 'CONTENT NOT FOUND'], 404);
    }

    if (array_key_exists('name', $data)) {
      $media_tag->setName($data['name']);
    }
    if (array_key_exists('pid', $data)) {
      $media_tag->parent->setValue($data['pid']);
    }
    if (array_key_exists('weight', $data)) {
      $media_tag->setWeight($data['weight']);
    }
    $media_tag->save();
    return new JsonResponse(['errCode' => 0, 'data' => self::serialize($media_tag)]);
  }

  public function deleteMediaTag($media_tag) {
    if (!$media_tag) {
      return new JsonResponse(['errCode' => 404, 'errMsg' => 'CONTENT NOT FOUND'], 404);
    }
    try {
      $media_storage = $this->entityTypeManager()->getStorage('media');
      $query = $media_storage->getQuery();
      $query->accessCheck(TRUE);
      $query->condition('field_tags', $media_tag);
      $media_ids = $query->execute();

      $medias = $media_storage->loadMultiple($media_ids);
      foreach ($medias as $media) {
        $media->field_tags->setValue(null);
        $media->save();
      }
      $media_tag->delete();
    }
    catch (\Exception $exception) {}
    return new JsonResponse(['errCode' => 0]);
  }

  /**
   * @param bool $assoc return data as array.
   *
   * @return mixed
   */
  private function getRequestData() {
    $request = \Drupal::request();
    $content = $request->getContent();
    $data = Json::decode($content);
    return $data;
  }

  public static function serialize(Term $term) {
    $data = [
      'id' => $term->id(),
      'title' => $term->getName(),
      'weight' => $term->getWeight(),
      'pid' => $term->parent->target_id,
    ];
    return $data;
  }

  protected function buildTree($flatten, $pidKey = 'pid', $idKey = 'id') {
    $parents = array();
    foreach ($flatten as $item){
      $parents[$item[$pidKey]][] = $item;
    }
    $fnBuilder = function($items, $parents, $idKey) use (&$fnBuilder) {
      foreach ($items as $position => $item) {
        $id = $item[$idKey];
        if(isset($parents[$id])) { //is the parent set
          $item['children'] = $fnBuilder($parents[$id], $parents, $idKey); //add children
        }
        //reset the value as children have changed
        $items[$position] = $item;
      }
      return $items;
    };
    $parent = !empty($parents[0]) ? $parents[0] : [];
    return $fnBuilder($parent, $parents, $idKey);
  }
}
