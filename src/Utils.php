<?php

namespace Drupal\viz_media;

use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\file\Entity\File;

class Utils {

  public static function generateThumbnail($fid) {
    if ($file = File::load($fid)) {
      /** @var \Drupal\Core\File\FileSystem $file_system */
      $file_system = \Drupal::service('file_system');
      list($type) = explode('/', $file->getMimeType());
      if ($type == 'image') {
        $uri = $file->getFileUri();
        /** @var \Drupal\Core\Image\Image $image */
        $image = \Drupal::service('image.factory')->get($uri);
        $w = min(600, $image->getWidth());
        $h = min(400, $image->getHeight());
        $image->scaleAndCrop($w, $h);
        // save to same directory
        $dirname = $file_system->dirname($uri);
        \Drupal::service('file_system')->prepareDirectory($dirname);
        // add thumbnail as suffix
        $filename = $file_system->basename($uri);
        $parts = explode('.', $filename);
        array_splice($parts, -1, 0, 'thumbnail');
        $filename = implode('.', $parts);
        $new_uri = "$dirname/$filename";

        if ($image->save($new_uri)) {
          $file = File::create([
            'filename' => $filename,
            'uri' => $new_uri
          ]);
          $file->save();
          return $file->id();
        }
      }
      if ($type == 'video') {
        $uri = $file->getFileUri();
        if (StreamWrapperManager::getScheme($uri) == 'oss') {
          $url = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);
          $url .= '?x-oss-process=video/snapshot,t_7000,f_jpg,m_fast';
          $file_parts = explode('.', $uri);
          array_splice($file_parts, -1, 1, 'thumbnail.jpg');
          $file = system_retrieve_file($url, implode('.', $file_parts),TRUE);
          if ($file) {
            return $file->id();
          }
        }
      }
    }
    return FALSE;
  }

}